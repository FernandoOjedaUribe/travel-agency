import { Component, OnInit, Inject } from '@angular/core';
import { Travel } from '../shared/Travel';
import { TravelService } from '../services/travel.service';
import { flyInOut, expand } from '../animations/app.animation';

@Component({
  selector: 'app-menu',
  templateUrl: './travel.component.html',
  styleUrls: ['./travel.component.scss'],
  host: {
    '[@flyInOut]': 'true',
    'style': 'display: block;'
    },
  animations: [
      flyInOut(),
      expand()
  ] 
})
export class TravelComponent implements OnInit {

  travels: Travel[];
  errMess: string;

  constructor(private travelService: TravelService,
    @Inject('BaseURL') public BaseURL) { }

  ngOnInit(): void {
    this.travelService.getTravels()
      .subscribe(travels => this.travels = travels,
        errmess => this.errMess = <any>errmess);
  }

}
