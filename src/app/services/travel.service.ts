import { Injectable } from '@angular/core';
import { Travel } from '../shared/Travel';
import { Observable, of } from 'rxjs';
import { delay } from 'rxjs/operators';
import { map, catchError } from 'rxjs/operators';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { baseURL } from '../shared/baseurl';
import { ProcessHTTPMsgService } from './process-httpmsg.service';

@Injectable({
  providedIn: 'root'
})
export class TravelService {
  dish : Travel;
  
  constructor(private http: HttpClient,
    private processHTTPMsgService: ProcessHTTPMsgService) { }

  getTravels(): Observable<Travel[]> {
    return this.http.get<Travel[]>(baseURL + 'travels')
    .pipe(catchError(this.processHTTPMsgService.handleError));
  }

  getTravel(id: string): Observable<Travel> {
    return this.http.get<Travel>(baseURL + 'travels/' + id)
    .pipe(catchError(this.processHTTPMsgService.handleError));
  }

  getFeaturedTravel(): Observable<Travel> {
    return this.http.get<Travel[]>(baseURL + 'travels?featured=true').pipe(map(travels => travels[0]))
    .pipe(catchError(this.processHTTPMsgService.handleError));
  }

  getTravelIds(): Observable<string[] | any> {
    return this.getTravels().pipe(map(travels => travels.map(travel => travel.id)))
    .pipe(catchError(error => error));
  }

  putTravel(dish: Travel): Observable<Travel> {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json'
      })
    };
    return this.http.put<Travel>(baseURL + 'travels/' + dish.id, dish, httpOptions)
      .pipe(catchError(this.processHTTPMsgService.handleError));

  }
}
